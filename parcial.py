#TALLER PARCIAL
import pandas as pd
import csv
import numpy
#1.importar archivo csv
archivo= pd.read_csv("CSV.csv", delimiter=",", encoding="ISO-8859-1")
lector=csv.reader(archivo)
print(archivo)
#2. Elaborar un programa, en el lenguaje de programación que se está trabajando este semestre, que lea el archivo y contenga un conjunto de funciones que:
#A. Calcule el valor promedio por metro cuadrado de un municipio dado, es decir, el nombre del municipio es un parámetro de la función.
from select import select
lista=[]
with open("CSV.csv")  as csv_file:
    csv_reader=csv.reader(csv_file, delimiter=',')
    line_count=0
    for row in csv_reader:
        if line_count> 0:
            predio={"Cogigo_predio": row[0],"Area":row[1],"Municipio":row[2],"Precio":row[3],"Tipo":row[4]} 
            lista.append(predio)
        line_count +=1
dicMunicipio={}
for p in lista:
		if p["Municipio"] in dicMunicipio:
			dicMunicipio[p["Municipio"]]["Area"]  += p["Area"]
			dicMunicipio[p["Municipio"]]["Precio"] += p["Precio"]
		else:
			dicMunicipio[p["Municipio"]] = {"Area":p["Area"], "Precio":p["Precio"]}
	
for k,v in dicMunicipio.items():
        precio =v["Precio"]
        area = v["Area"]
        promedio=int(precio)/int(area)
        print ("Del municipio de " +k +" su valor promedio por metro cuadrado es=" + str(promedio))                     
#B.Retorne una lista ordenada de precios para un municipio y tipo dado, es decir, el municipio y el tipo son parámetros de la función.
import csv
archivo=open("CSV.csv",encoding="ISO-8859-1")
lector=csv.reader(archivo,delimiter=",")
listas_x=[]
listaTipos=[]
for fila in  lector:
    if fila[2]=="Bogota":
        if not(fila[3] in listas_x):
           listas_x.append(fila[3])
           if fila[4]=="Casa":
                if not(fila[3] in listaTipos):
                    listaTipos.append(fila[3])
for tipo in listaTipos:
    print("De Bogotá el precio por las casas es de=",tipo)
#C.Retorne una lista ordenada de los precios que sean iguales o mayores a un valor dado para un municipio y tipo específico. Tanto el valor, como el nombre de municipio y tipo son parámetros de la función.
archivo=open("CSV.csv", encoding="ISO-8859-1")
lector=csv.reader(archivo,delimiter=",")
listaCostos=[]
for fila in lector:
    if fila[2]=="Medellin":
        if fila[4]=="Apartamento":
            if fila[3]>="300":
                if not (fila[3] in listaCostos):
                    listaCostos.append(fila[3])
for precio in listaCostos:
    print("Precios iguales o mayores de Medellin=", precio) 
#D.Retorne el promedio del área de todos los predios de la tabla.
archivo= pd.read_csv("CSV.csv", delimiter=",", encoding="ISO-8859-1")
lector=csv.reader(archivo)
promedio=numpy.mean(archivo["Area"])
print("El promedio total del area es=",promedio)
#E.Retorne una lista ordenada de los municipios que tienen predios registrados en la tabla. Los nombres no pueden ir repetidos.
ValoresNoRepetidos=[]
for element in archivo.Municipio:
    if element not in ValoresNoRepetidos:
        ValoresNoRepetidos.append(element)
print("Los Municipios Son=",ValoresNoRepetidos)    
#F.Retorne una tupla con las áreas de los predios de un tipo dado. El tipo es un parámetro de la función.
import pandas as pd
datos=pd.read_csv("CSV.csv",encoding="ISO-8859-1")

def area_predios(tipo:str):
    return list (datos[datos.Tipo==tipo].Area.unique())

print (area_predios(input("ingrese el tipo")))
#G.Retorne un diccionario con los datos completos de un predio dado. El código del predio es un parámetro de la función.
dict_from_csv=pd.read_csv("CSV.csv").to_dict()
print(dict_from_csv)
